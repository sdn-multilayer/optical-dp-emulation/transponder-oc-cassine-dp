from setuptools import setup, find_packages

with open('./requirements.txt') as reqs_txt:
    requirements = [line.strip() for line in reqs_txt]


setup(
    name = 'cassinidp',
    version = '0.1',
    packages = find_packages(),
    url = 'https://gitlab.com/sdn-multilayer/optical-dp-emulation/transponder-oc-cassine-dp',
    license = 'APACHE 2',
    author = 'Fernando Farias ',
    author_email = 'fernando.farias@rnp.br',
    description = 'Emulator of Transponder Cassini ',
    python_requires='>=3.6.*',
    install_requires=requirements
)
