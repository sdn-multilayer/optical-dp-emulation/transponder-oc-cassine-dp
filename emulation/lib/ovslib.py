"""
This library has worked with ovs commands inside of container
to manipulate action of the ovs (bridge, ports , interface and etc..).
"""


def run_cmd(node, cmd):
    ret, out = node.exec_cmd(cmd)
    if ret != 0:
        raise RuntimeError(out)
    else:
        return out


def add_port_ovs(node, br, port):
    cmd = "ovs-vsctl add-port {} {}".format(br, port)
    run_cmd(node, cmd)


def add_tag_port_ovs(node, br, port, tag):
    cmd = "ovs-vsctl set port {} {} tag={}".format(br, port, tag)
    run_cmd(node, cmd)


def add_trunks_port_ovs(node, br, port, trunks):
    cmd = "ovs-vsctl set port {} {} trunks={}".format(br, port, trunks)
    run_cmd(node, cmd)


def del_port_ovs(node, br, port):
    cmd = "ovs-vsctl del-port {} {}".format(br, port)
    run_cmd(node, cmd)


def exist_port(node, port):
    cmd = "ovs-vsctl get port {} name".format(port)
    try:
        run_cmd(node, cmd)
        return True
    except Exception:
        return False


def get_list_port(node, br):
    cmd = "ovs-vsctl list-ports {}".format(br)
    out = run_cmd(node, cmd)
    ports = out.split().strip()
    return ports


def get_tag_port(node, port):
    cmd = "ovs-vsctl get port {} tag".format(port)
    ret = run_cmd(node, cmd)
    tag = str(ret).encode("ascii")
    if tag.__eq__("[]"):
        return None
    return tag


def get_trunks_port(node, port):
    cmd = "ovs-vsctl get port {} trunks".format(port)
    ret = run_cmd(node, cmd)
    trunk = str(ret).encode("ascii")
    if trunk.__eq__("[]"):
        return None
    return trunk

