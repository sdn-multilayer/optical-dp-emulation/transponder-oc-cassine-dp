import logging
import os
import docker

logger = logging.getLogger(__name__)


class Node(object):
    def __init__(self, name, image, url = "localhost"):
        self.name = name
        self.image = image
        self.container = None
        self.client = url

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, value):
        self.__name=value

    @property
    def client(self):
        return self._client

    @client.setter
    def client(self, v):
        if v.__eq__("localhost"):
            self._client = docker.DockerClient(base_url='unix://var/run/docker.sock')
        else:
            self._client = docker.DockerClient(base_url=v)

    @property
    def pid(self):
        return self.container.attrs["State"]["Pid"]

    @property
    def ctlip(self):
        return self.container.attrs["NetworkSettings"]["IPAddress"]

    def init(self):
        conf = dict()
        conf.update(
            detach = True,
            cap_add = ["ALL"],
            privileged = True,
            tty = True,
            name = self.name,
            hostname = self.name,
            environment = ["container=Docker"],
        )
        return conf.copy()

    def start(self, **kwargs):
        self.client.containers.run(self.image, **kwargs)
        self.container = self.client.containers.get(self.name)
        os.makedirs("/var/run/netns", exist_ok = True)
        os.symlink("/proc/{pid}/ns/net".format(pid = self.get_pid()), "/var/run/netns/{name}".format(name = self.name))

    def stop(self):
        self.container.remove(force = True)
        os.remove("/var/run/netns/{name}".format(name = self.name))

    def get_pid(self):
        pid = self.container.attrs["State"]["Pid"]
        return pid

    def get_ip_ctl(self):
        ip = self.container.attrs["NetworkSettings"]["IPAddress"]
        return ip

    def exec_cmd(self, cmd):
        return self.container.exec_run(cmd = cmd, tty = True, privileged = True)

    def add_port(self,name, **kwargs):
        pass

    def rem_port(self, name, **kwargs):
        pass

