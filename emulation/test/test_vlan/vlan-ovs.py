import logging
from emulation import Link
from emulation import Switch, Host

if __name__ == '__main__':

    sw1 = Switch("sw1")
    sw2 = Switch("sw2")
    ht1 = Host("ht1")
    ht2 = Host("ht2")


    try:
        sw1.start()
        sw2.start()
        ht1.start()
        ht2.start()

        link1 = Link(sw1.name, sw2.name)
        link1.start()

        link2 = Link(ht1.name, sw1.name)
        link2.start()

        link3 = Link(ht2.name, sw2.name)
        link3.start()

        while True:
            pass
    except KeyboardInterrupt:
        logging.warning("Exiting")
        sw1.stop()
        sw2.stop()
