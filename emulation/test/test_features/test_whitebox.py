from emulation.model.switch.whitebox import Whitebox
from emulation.model.link.ethernet import Ethernet
import time

import logging

logging.basicConfig(level=logging.INFO,
                    format='[%(levelname)s] :: [%(asctime)s] :: [%(module)s %(lineno)d] :: %(message)s')

if __name__ == '__main__':

        wh1 = Whitebox("wh1")
        wh2 = Whitebox("wh2")

        wh1.run()
        wh2.run()

        lnk1 = Ethernet(wh1, wh2)
        lnk1.run()

        time.sleep(90)

        wh1.delete()
        wh2.delete()