from emulation.node import Node
from pyroute2 import IPRoute, NetNS

def startup_if(ns, ifname):
    with NetNS(netns = ns) as netns:
        ifnet = netns.link_lookup(ifname=ifname)[0]
        netns.link("set", index = ifnet, state = "up")

def create_veth_netns(src:Node, dst:Node):
    if_src = "{}-{}".format(src.name,dst.name)
    if_dst = "{}-{}".format(dst.name,src.name)

    with IPRoute() as ipr:
        ipr.link("add",
                 ifname=if_src,
                 kind="veth",
                 net_ns_fd=src.name,
                 peer={"ifname": if_dst, "net_ns_fd": dst.name})

    startup_if(src.name, if_src)
    startup_if(dst.name, if_dst)

    return (if_src,if_dst)

def delete_veth_netns(ns:Node, ifname):
    with NetNS(netns = ns.name) as netns:
        ifnet = netns.link_lookup(ifname=ifname)[0]
        netns.link("del", index = ifnet)



