from emulation.link import Link


class Fibre(Link):
    def __init__(self, src, dst):
        super().__init__(src, dst)

    @property
    def type(self):
        return "optical"

    def run(self):
        pass


    def remove(self):
        pass

