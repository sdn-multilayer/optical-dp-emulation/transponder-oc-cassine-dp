from emulation.link import Link
import emulation.model.link.util as tools
import logging

logger = logging.getLogger(__name__)

"""
Simple Ethernet link using veth pair
"""
class Ethernet(Link):
    def __init__(self, src, dst):
        super().__init__(src, dst)
        self.if_src = ""
        self.if_dst = ""

    @property
    def type(self):
        return "ethernet"

    def run(self):
        try:
            self.if_src, self.if_dst = tools.create_veth_netns(self.src, self.dst)
            self.src.add_port(self.if_src, type=self.type)
            self.dst.add_port(self.if_dst, type=self.type)
        except Exception as ex:
            logger.error(ex)

    def remove(self):
        try:
            self.src.rem_port(self.if_src, type=self.type)
            self.dst.rem_port(self.if_dst, type=self.type)
            tools.delete_veth_netns(self.src, self.if_src)
        except Exception as ex:
            logger.error(ex)

