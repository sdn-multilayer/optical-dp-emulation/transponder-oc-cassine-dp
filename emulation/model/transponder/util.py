import emulation.model.switch.util as tools_sw


def add_och_port(node, och_channel, och_ifname):
    tools_sw.add_port_ovs(node, br = och_channel, port = och_ifname)


def rem_och_port(node, och_channel, och_ifname):
    tools_sw.rem_port_ovs(node, br = och_channel, port = och_ifname)


def add_eth_port(node, eth_channel, eth_ifname):
    tools_sw.add_port_ovs(node, br = eth_channel, port = eth_ifname)


def rem_eth_port(node, eth_channel, eth_ifname):
    tools_sw.rem_port_ovs(node, br = eth_channel, port = eth_ifname)


def get_oe_frequencies(node, och_channel):
    freqs = []
    ports = tools_sw.list_ports_bridge_ovs(node, och_channel)
    for port in ports:
        tag = tools_sw.get_port_tag_ovs(node, port)
        if tag is not None:
            freqs.append(tag)
    return freqs


def sync_och_frequencies(node, freqs, och_ifname):
    tools_sw.set_trunks_port_ovs(node, och_ifname, ",".join(freqs))

def exit_port(node, port):
    tools_sw.