from emulation.node import Node
from itertools import count
import logging
import time

import emulation.model.switch.util as tools_sw
import emulation.model.transponder.util as tools_och

logger = logging.getLogger(__name__)

class Cassini(Node):
    def __init__(self, name):
        self.och_it = count(0)
        super().__init__(name, image="sdnml/transp-cassini-dp")

    def run(self):
        conf = self.init()
        conf.update(volumes = {'/lib/modules': {'bind': '/lib/modules', 'mode': 'rw'}})
        super(Cassini, self).start(**conf)
        while True:
            if tools_sw.is_working(self):
                logger.info("cassini transponder has started")
                break
            time.sleep(2)

    def add_port(self, name, **kwargs):
        try:
            type = kwargs.get("type")
            if type.__eq__("optical"):
                och_channel= kwargs.get("och_channel")
                och_ifname = kwargs.get("och_ifname")
                tools_och.add_och_port(self, och_channel,och_ifname)
                freqs = tools_och.get_oe_frequencies(self,och_channel)
                tools_och.sync_och_frequencies(self,freqs,och_ifname)

            elif type.__eq__("ethernet"):
                eth_channel = kwargs.get("eth_channel")
                eth_ifname = kwargs.get("eth_ifname")
                tools_och.add_eth_port(self, eth_channel, eth_ifname)
            else:
                logger.warning("type of port was not found")
                logger.error("cannot adds port")
        except Exception as ex:
            logger.error(ex)

    def rem_port(self, name, **kwargs):
        try:
            type = kwargs.get("type")
            if type.__eq__("optical"):
                och_channel= kwargs.get("och_channel")
                och_ifname = kwargs.get("och_ifname")
                tools_och.rem_och_port(self,och_channel,och_ifname)
            elif type.__eq__("ethernet"):
                eth_channel = kwargs.get("eth_channel")
                eth_ifname = kwargs.get("eth_ifname")
                tools_och.rem_eth_port(self,eth_channel,eth_ifname)
            else:
                logger.warning("type of port was not found")
                logger.error("cannot adds port")
        except Exception as ex:
            logger.error(ex)


    def get_port(self):
        pass










