from emulation.node import Node


def run_cmd(node: Node, cmd) -> str:
    ret, out = node.exec_cmd(cmd)
    if ret != 0:
        raise RuntimeError(out)
    else:
        return str(out)


## OVSLIB

def add_bridge_ovs(node, br):
    cmd = "ovs-vsctl add-br {}".format(br)
    run_cmd(node, cmd)


def rem_bridge_ovs(node, br):
    cmd = "ovs-vsctl del-port {}".format(br)
    run_cmd(node, cmd)


def add_port_ovs(node, br, port):
    cmd = "ovs-vsctl add-port {} {}".format(br, port)
    run_cmd(node, cmd)


def rem_port_ovs(node, br, port):
    cmd = "ovs-vsctl del-port {} {}".format(br, port)
    run_cmd(node, cmd)


def list_ports_bridge_ovs(node, br):
    cmd = "ovs-vsctl list-ports {}".format(br)
    ret = run_cmd(node, cmd)
    ports = ret.split()
    return ports


def get_port_tag_ovs(node, port):
    cmd = "ovs-vsctl get port {} tag".format(port)
    ret = run_cmd(node, cmd)
    tag = ret
    if tag.__eq__("[]"):
        return None
    return tag


def get_port_trunks_ovs(node, port):
    cmd = "ovs-vsctl get port {} trunks".format(port)
    ret = run_cmd(node, cmd)
    trunks = ret.split(",")
    if trunks.__eq__("[]"):
        return None

    return trunks


def set_tag_port_ovs(node, port, tag):
    cmd = "ovs-vsctl set port {} tag={}".format(port, tag)
    run_cmd(node, cmd)


def set_trunks_port_ovs(node, port, trunks):
    cmd = "ovs-vsctl set port {} trunks={}".format(port, trunks)
    run_cmd(node, cmd)

def is_working(node):
    cmd = "ovs-vsctl get Open_vSwitch . ovs_version"
    try:
        run_cmd(node, cmd)
        return True
    except Exception:
        return False
