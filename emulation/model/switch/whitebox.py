from emulation.node import Node
import emulation.model.switch.util as tools

import logging
import time


logger = logging.getLogger(__name__)

class Whitebox(Node):
    def __init__(self,name):
        super().__init__(name, image = "vsdn/whitebox")

    @property
    def br_oper(self):
        return "br0"

    def run(self):
        try:
            conf = self.init()
            conf.update(volumes = {'/lib/modules': {'bind': '/lib/modules', 'mode': 'rw'}})
            super(Whitebox, self).start(**conf)
            while True:
                if tools.is_working(self):
                    logger.info("the processes in {} have started".format(self.name))
                    break
                time.sleep(2)
            self.add_bridge(self.br_oper)
            logger.info("whitebox ({}) was started".format(self.name))
        except Exception as ex:
            logger.error(ex)

    def delete(self):
        try:
            super(Whitebox, self).stop()
            logger.info("whitebox ({}) was deleted".format(self.name))
        except Exception as ex:
            logger.error(ex)

    def add_bridge(self, name):
        try:
            tools.add_bridge_ovs(self, name)
        except Exception as ex:
            logger.error(ex)

    def rem_bridge(self, name):
        try:
            tools.rem_bridge_ovs(self, name)
        except Exception as ex:
            logger.error(ex)

    def add_port(self,name,**kwargs):
        try:
            br = kwargs.get("br")
            if br is not None:
                tools.add_port_ovs(self,br,name)
            else:
                tools.add_port_ovs(self,self.br_oper, name)
        except Exception as ex:
            logger.error(ex)

    def rem_port(self,name, **kwargs):
        try:
            br = kwargs.get("br")
            if br is not None:
                tools.rem_port_ovs(self,br,name)
            else:
                tools.rem_port_ovs(self, self.br_oper,name)
        except Exception as ex:
            logger.error(ex)

