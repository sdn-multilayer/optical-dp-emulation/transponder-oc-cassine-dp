from emulation import Node
class SimpleHost(Node):
    def __init__(self, name):
        super().__init__(name, image = "vsdn/host")

    def run(self):
        conf = super(SimpleHost, self).gen_conf()
        super(SimpleHost, self).start(**conf)